package vn.dangduy.sort;

public class InsertionSort {
	public static void printArray(int[] input) {
		int inputSize = input.length;
		for (int i = 0; i < inputSize; i++)
			System.out.print(input[i] + " ");
	}

	public static void swap(int i, int j, int[] input) {
		int temp = input[i];
		input[i] = input[j];
		input[j] = temp;
	}

	public static int[] insertionSortAlgorithm(int[] input) {
		for (int i = 0; i < input.length - 1; i++)
			for (int j = i + 1; j < input.length; j++)
				if (input[i] > input[j])
					swap(i, j, input);

		return input;
	}

	public static void main(String[] args) {
		int[] input = { 6, 4, 2, 9, 3 };
		printArray(insertionSortAlgorithm(input));
	}
}
